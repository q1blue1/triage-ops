# frozen_string_literal: true

require 'erb'
require 'fileutils'

require_relative '../group_definition'

module Generate
  module GroupPolicy
    def self.run(options)
      template_name = File.basename(options.template)
      erb = ERB.new(File.read(options.template), trim_mode: '-')

      FileUtils.rm_rf("#{destination}/#{template_name}", secure: true)
      FileUtils.mkdir_p("#{destination}/#{template_name}")

      GroupDefinition::DATA.each do |name, definition|
        next if options.only && !options.only.include?(name)
        
        next if definition['ignore_templates']&.include?(options.template)

        group_method_name = "group_#{name}".tr('-', '_')
        group = GroupDefinition.public_send(group_method_name)

        File.write(
          "#{destination}/#{template_name}/#{name}.yml",
          erb.result_with_hash(
            group_method_name: group_method_name,
            group_label_name: group.dig(:labels, 0),
            stage_label_name: group.dig(:stage, 'label'),
            extra_labels: group[:labels][1..],
            default_labeling: group[:default_labeling]
          ).gsub(/REMOVE ME\n/, '')
        )
      end
    end

    def self.destination
      @destination ||=
        File.expand_path('generated', "#{__dir__}/../../policies")
    end
  end
end
